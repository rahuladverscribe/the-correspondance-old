@extends('layouts.panel')

@section('content')
    <div class="pcoded-main-container">
        <div class="pcoded-content">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <div class="page-header-title">
                                <h5 class="m-b-10">Home</h5>
                            </div>
                            {{--<ul class="breadcrumb">--}}
                            {{--<li class="breadcrumb-item"><a href="#"><i class="feather icon-home"></i></a>--}}
                            {{--</li>--}}
                            {{--<li class="breadcrumb-item"><a href="#!">Dashboard</a></li>--}}
                            {{--</ul>--}}
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <!-- [ Main Content ] start -->
            <div class="row">
                <!-- customar project  start -->
                <div class="row col-xl-12 col-md-12">
                    <div class="col-xl-1">
                        <img class="img-fluid card-img-top"
                             src="{{asset('panel/assets/images/slider/img-slide-2.jpg')}}"
                                                        alt="Card image cap">
                    </div>
                    <div class="col-xl-10">
                        <div class="row">
                            <div class="card-deck">
                                <div class="col-xl-4 col-md-6 mb-5">
                                    <div class="card">
                                        <img class="img-fluid card-img-top"
                                             src="{{asset('panel/assets/images/slider/img-slide-2.jpg')}}"
                                             alt="Card image cap">
                                        <div class="card-body">
                                            <h5 class="card-title">Card title</h5>
                                            <p class="card-text">This is a wider card with supporting text below as a
                                                natural
                                                lead-in to additional content. This content is a little bit longer.</p>
                                            <p class="card-text">
                                                <small class="text-muted">Last updated 3 mins ago</small>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-6 mb-5">
                                    <div class="card">
                                        <img class="img-fluid card-img-top"
                                             src="{{asset('panel/assets/images/slider/img-slide-2.jpg')}}"
                                             alt="Card image cap">
                                        <div class="card-body">
                                            <h5 class="card-title">Card title</h5>
                                            <p class="card-text">This is a wider card with supporting text below as a
                                                natural
                                                lead-in to additional content. This content is a little bit longer.</p>
                                            <p class="card-text">
                                                <small class="text-muted">Last updated 3 mins ago</small>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-6 mb-5">
                                    <div class="card">
                                        <img class="img-fluid card-img-top"
                                             src="{{asset('panel/assets/images/slider/img-slide-2.jpg')}}"
                                             alt="Card image cap">
                                        <div class="card-body">
                                            <h5 class="card-title">Card title</h5>
                                            <p class="card-text">This is a wider card with supporting text below as a
                                                natural
                                                lead-in to additional content. This content is a little bit longer.</p>
                                            <p class="card-text">
                                                <small class="text-muted">Last updated 3 mins ago</small>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-6 mb-5">
                                    <div class="card">
                                        <img class="img-fluid card-img-top"
                                             src="{{asset('panel/assets/images/slider/img-slide-2.jpg')}}"
                                             alt="Card image cap">
                                        <div class="card-body">
                                            <h5 class="card-title">Card title</h5>
                                            <p class="card-text">This is a wider card with supporting text below as a
                                                natural
                                                lead-in to additional content. This content is a little bit longer.</p>
                                            <p class="card-text">
                                                <small class="text-muted">Last updated 3 mins ago</small>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-6 mb-5">
                                    <div class="card">
                                        <img class="img-fluid card-img-top"
                                             src="{{asset('panel/assets/images/slider/img-slide-2.jpg')}}"
                                             alt="Card image cap">
                                        <div class="card-body">
                                            <h5 class="card-title">Card title</h5>
                                            <p class="card-text">This is a wider card with supporting text below as a
                                                natural
                                                lead-in to additional content. This content is a little bit longer.</p>
                                            <p class="card-text">
                                                <small class="text-muted">Last updated 3 mins ago</small>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-6 mb-5">
                                    <div class="card">
                                        <img class="img-fluid card-img-top"
                                             src="{{asset('panel/assets/images/slider/img-slide-2.jpg')}}"
                                             alt="Card image cap">
                                        <div class="card-body">
                                            <h5 class="card-title">Card title</h5>
                                            <p class="card-text">This is a wider card with supporting text below as a
                                                natural
                                                lead-in to additional content. This content is a little bit longer.</p>
                                            <p class="card-text">
                                                <small class="text-muted">Last updated 3 mins ago</small>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-1">
                        <img class="img-fluid card-img-top"
                                                        src="{{asset('panel/assets/images/slider/img-slide-2.jpg')}}"
                                                        alt="Card image cap">
                    </div>
                </div>
            <?php /*
                <div class="col-xl-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center m-l-0">
                                <div class="col-auto">
                                    <i class="icon feather icon-book f-30 text-c-purple"></i>
                                </div>
                                <div class="col-auto">
                                    <h6 class="text-muted m-b-10">Tickets Answered</h6>
                                    <h2 class="m-b-0">379</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center m-l-0">
                                <div class="col-auto">
                                    <i class="icon feather icon-navigation-2 f-30 text-c-green"></i>
                                </div>
                                <div class="col-auto">
                                    <h6 class="text-muted m-b-10">Projects Launched</h6>
                                    <h2 class="m-b-0">205</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center m-l-0">
                                <div class="col-auto">
                                    <i class="icon feather icon-users f-30 text-c-red"></i>
                                </div>
                                <div class="col-auto">
                                    <h6 class="text-muted m-b-10">Happy Customers</h6>
                                    <h2 class="m-b-0">5984</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center m-l-0">
                                <div class="col-auto">
                                    <i class="icon feather icon-award f-30 text-c-blue"></i>
                                </div>
                                <div class="col-auto">
                                    <h6 class="text-muted m-b-10">Unique Innovation</h6>
                                    <h2 class="m-b-0">325</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                */ ?>
            <!-- Our Journalists -->

                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Our Journalists</h5>
                        </div>
                        <div class="card-body">
                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <div class="row">
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card user-card user-card-1 support-bar1">
                                                    <div class="card-header border-0 p-2 pb-0">
                                                        <div class="cover-img-block">
                                                            <img src="{{asset('panel/assets/images/widget/slider7.jpg')}}"
                                                                 alt=""
                                                                 class="img-fluid">
                                                        </div>
                                                    </div>
                                                    <div class="card-body pt-0">
                                                        <div class="user-about-block text-center">
                                                            <div class="row align-items-end">
                                                                <div class="col"></div>
                                                                <div class="col">
                                                                    <div class="position-relative d-inline-block">
                                                                        <img class="img-radius img-fluid wid-80"
                                                                             src="{{asset('panel/assets/images/user/avatar-2.jpg')}}"
                                                                             alt="User image">
                                                                    </div>
                                                                </div>
                                                                <div class="col"></div>
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <h6 class="mb-1 mt-3">Josephin Doe</h6>
                                                            <p class="mb-3 text-muted">UI/UX Designer</p>
                                                            <p class="mb-1">Lorem Ipsum is simply dummy text</p>
                                                            <p class="mb-0">been the industry's standard</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card user-card user-card-1 support-bar1">
                                                    <div class="card-header border-0 p-2 pb-0">
                                                        <div class="cover-img-block">
                                                            <img src="{{asset('panel/assets/images/widget/slider7.jpg')}}"
                                                                 alt=""
                                                                 class="img-fluid">
                                                        </div>
                                                    </div>
                                                    <div class="card-body pt-0">
                                                        <div class="user-about-block text-center">
                                                            <div class="row align-items-end">
                                                                <div class="col"></div>
                                                                <div class="col">
                                                                    <div class="position-relative d-inline-block">
                                                                        <img class="img-radius img-fluid wid-80"
                                                                             src="{{asset('panel/assets/images/user/avatar-2.jpg')}}"
                                                                             alt="User image">
                                                                    </div>
                                                                </div>
                                                                <div class="col"></div>
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <h6 class="mb-1 mt-3">Josephin Doe</h6>
                                                            <p class="mb-3 text-muted">UI/UX Designer</p>
                                                            <p class="mb-1">Lorem Ipsum is simply dummy text</p>
                                                            <p class="mb-0">been the industry's standard</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card user-card user-card-1 support-bar1">
                                                    <div class="card-header border-0 p-2 pb-0">
                                                        <div class="cover-img-block">
                                                            <img src="{{asset('panel/assets/images/widget/slider7.jpg')}}"
                                                                 alt=""
                                                                 class="img-fluid">
                                                        </div>
                                                    </div>
                                                    <div class="card-body pt-0">
                                                        <div class="user-about-block text-center">
                                                            <div class="row align-items-end">
                                                                <div class="col"></div>
                                                                <div class="col">
                                                                    <div class="position-relative d-inline-block">
                                                                        <img class="img-radius img-fluid wid-80"
                                                                             src="{{asset('panel/assets/images/user/avatar-2.jpg')}}"
                                                                             alt="User image">
                                                                    </div>
                                                                </div>
                                                                <div class="col"></div>
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <h6 class="mb-1 mt-3">Josephin Doe</h6>
                                                            <p class="mb-3 text-muted">UI/UX Designer</p>
                                                            <p class="mb-1">Lorem Ipsum is simply dummy text</p>
                                                            <p class="mb-0">been the industry's standard</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card user-card user-card-1 support-bar1">
                                                    <div class="card-header border-0 p-2 pb-0">
                                                        <div class="cover-img-block">
                                                            <img src="{{asset('panel/assets/images/widget/slider7.jpg')}}"
                                                                 alt=""
                                                                 class="img-fluid">
                                                        </div>
                                                    </div>
                                                    <div class="card-body pt-0">
                                                        <div class="user-about-block text-center">
                                                            <div class="row align-items-end">
                                                                <div class="col"></div>
                                                                <div class="col">
                                                                    <div class="position-relative d-inline-block">
                                                                        <img class="img-radius img-fluid wid-80"
                                                                             src="{{asset('panel/assets/images/user/avatar-2.jpg')}}"
                                                                             alt="User image">
                                                                    </div>
                                                                </div>
                                                                <div class="col"></div>
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <h6 class="mb-1 mt-3">Josephin Doe</h6>
                                                            <p class="mb-3 text-muted">UI/UX Designer</p>
                                                            <p class="mb-1">Lorem Ipsum is simply dummy text</p>
                                                            <p class="mb-0">been the industry's standard</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="row">
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card user-card user-card-1 support-bar1">
                                                    <div class="card-header border-0 p-2 pb-0">
                                                        <div class="cover-img-block">
                                                            <img src="{{asset('panel/assets/images/widget/slider7.jpg')}}"
                                                                 alt=""
                                                                 class="img-fluid">
                                                        </div>
                                                    </div>
                                                    <div class="card-body pt-0">
                                                        <div class="user-about-block text-center">
                                                            <div class="row align-items-end">
                                                                <div class="col"></div>
                                                                <div class="col">
                                                                    <div class="position-relative d-inline-block">
                                                                        <img class="img-radius img-fluid wid-80"
                                                                             src="{{asset('panel/assets/images/user/avatar-2.jpg')}}"
                                                                             alt="User image">
                                                                    </div>
                                                                </div>
                                                                <div class="col"></div>
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <h6 class="mb-1 mt-3">Josephin Doe</h6>
                                                            <p class="mb-3 text-muted">UI/UX Designer</p>
                                                            <p class="mb-1">Lorem Ipsum is simply dummy text</p>
                                                            <p class="mb-0">been the industry's standard</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card user-card user-card-1 support-bar1">
                                                    <div class="card-header border-0 p-2 pb-0">
                                                        <div class="cover-img-block">
                                                            <img src="{{asset('panel/assets/images/widget/slider7.jpg')}}"
                                                                 alt=""
                                                                 class="img-fluid">
                                                        </div>
                                                    </div>
                                                    <div class="card-body pt-0">
                                                        <div class="user-about-block text-center">
                                                            <div class="row align-items-end">
                                                                <div class="col"></div>
                                                                <div class="col">
                                                                    <div class="position-relative d-inline-block">
                                                                        <img class="img-radius img-fluid wid-80"
                                                                             src="{{asset('panel/assets/images/user/avatar-2.jpg')}}"
                                                                             alt="User image">
                                                                    </div>
                                                                </div>
                                                                <div class="col"></div>
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <h6 class="mb-1 mt-3">Josephin Doe</h6>
                                                            <p class="mb-3 text-muted">UI/UX Designer</p>
                                                            <p class="mb-1">Lorem Ipsum is simply dummy text</p>
                                                            <p class="mb-0">been the industry's standard</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card user-card user-card-1 support-bar1">
                                                    <div class="card-header border-0 p-2 pb-0">
                                                        <div class="cover-img-block">
                                                            <img src="{{asset('panel/assets/images/widget/slider7.jpg')}}"
                                                                 alt=""
                                                                 class="img-fluid">
                                                        </div>
                                                    </div>
                                                    <div class="card-body pt-0">
                                                        <div class="user-about-block text-center">
                                                            <div class="row align-items-end">
                                                                <div class="col"></div>
                                                                <div class="col">
                                                                    <div class="position-relative d-inline-block">
                                                                        <img class="img-radius img-fluid wid-80"
                                                                             src="{{asset('panel/assets/images/user/avatar-2.jpg')}}"
                                                                             alt="User image">
                                                                    </div>
                                                                </div>
                                                                <div class="col"></div>
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <h6 class="mb-1 mt-3">Josephin Doe</h6>
                                                            <p class="mb-3 text-muted">UI/UX Designer</p>
                                                            <p class="mb-1">Lorem Ipsum is simply dummy text</p>
                                                            <p class="mb-0">been the industry's standard</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card user-card user-card-1 support-bar1">
                                                    <div class="card-header border-0 p-2 pb-0">
                                                        <div class="cover-img-block">
                                                            <img src="{{asset('panel/assets/images/widget/slider7.jpg')}}"
                                                                 alt=""
                                                                 class="img-fluid">
                                                        </div>
                                                    </div>
                                                    <div class="card-body pt-0">
                                                        <div class="user-about-block text-center">
                                                            <div class="row align-items-end">
                                                                <div class="col"></div>
                                                                <div class="col">
                                                                    <div class="position-relative d-inline-block">
                                                                        <img class="img-radius img-fluid wid-80"
                                                                             src="{{asset('panel/assets/images/user/avatar-2.jpg')}}"
                                                                             alt="User image">
                                                                    </div>
                                                                </div>
                                                                <div class="col"></div>
                                                            </div>
                                                        </div>
                                                        <div class="text-center">
                                                            <h6 class="mb-1 mt-3">Josephin Doe</h6>
                                                            <p class="mb-3 text-muted">UI/UX Designer</p>
                                                            <p class="mb-1">Lorem Ipsum is simply dummy text</p>
                                                            <p class="mb-0">been the industry's standard</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a class="carousel-control-prev carousel-control" href="#carouselExampleControls"
                                   role="button"
                                   data-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span><span
                                            class="sr-only">Previous</span></a>
                                <a class="carousel-control-next carousel-control" href="#carouselExampleControls"
                                   role="button"
                                   data-slide="next"><span class="carousel-control-next-icon" aria-hidden="true"></span><span
                                            class="sr-only">Next</span></a>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Testimonials -->
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Testimonials</h5>
                        </div>
                        <div class="card-body">
                            <div id="carouseltestimonials" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner col-lg-10 col-md-12" style="margin-left: 8rem !important;">
                                    <div class="carousel-item active">
                                        <div class="card-body">
                                            <div class="review-block">
                                                <div class="row">
                                                    <div class="col-sm-auto p-r-0">
                                                        <img src="{{asset('panel/assets/images/user/avatar-2.jpg')}}"
                                                             alt="user image"
                                                             class="img-radius profile-img cust-img m-b-15">
                                                    </div>
                                                    <div class="col">
                                                        <h6 class="m-b-15">John Deo <span
                                                                    class="float-right f-13 text-muted"> a week ago</span>
                                                        </h6>
                                                        <a href="#!"><i
                                                                    class="feather icon-star-on f-18 text-c-yellow"></i></a>
                                                        <a href="#!"><i
                                                                    class="feather icon-star-on f-18 text-c-yellow"></i></a>
                                                        <a href="#!"><i
                                                                    class="feather icon-star-on f-18 text-c-yellow"></i></a>
                                                        <a href="#!"><i
                                                                    class="feather icon-star f-18 text-muted"></i></a>
                                                        <a href="#!"><i
                                                                    class="feather icon-star f-18 text-muted"></i></a>
                                                        <p class="m-t-15 m-b-15 text-muted">Lorem Ipsum is simply dummy
                                                            text of the printing and typesetting industry. Lorem Ipsum
                                                            has been the industry's standard dummy text ever since the
                                                            1500s, when an unknown printer
                                                            took a
                                                            galley of type and scrambled it to make a type specimen
                                                            book.</p>
                                                        <a href="#!" class="m-r-30 text-muted"><i
                                                                    class="feather icon-thumbs-up m-r-15"></i>Helpful?</a>
                                                        <a href="#!"><i
                                                                    class="feather icon-heart-on text-c-red m-r-15"></i></a>
                                                        <a href="#!"><i class="feather icon-edit text-muted"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="card-body">
                                            <div class="review-block">
                                                <div class="row">
                                                    <div class="col-sm-auto p-r-0">
                                                        <img src="{{asset('panel/assets/images/user/avatar-2.jpg')}}"
                                                             alt="user image"
                                                             class="img-radius profile-img cust-img m-b-15">
                                                    </div>
                                                    <div class="col">
                                                        <h6 class="m-b-15">John Deo <span
                                                                    class="float-right f-13 text-muted"> a week ago</span>
                                                        </h6>
                                                        <a href="#!"><i
                                                                    class="feather icon-star-on f-18 text-c-yellow"></i></a>
                                                        <a href="#!"><i
                                                                    class="feather icon-star-on f-18 text-c-yellow"></i></a>
                                                        <a href="#!"><i
                                                                    class="feather icon-star-on f-18 text-c-yellow"></i></a>
                                                        <a href="#!"><i
                                                                    class="feather icon-star f-18 text-muted"></i></a>
                                                        <a href="#!"><i
                                                                    class="feather icon-star f-18 text-muted"></i></a>
                                                        <p class="m-t-15 m-b-15 text-muted">Lorem Ipsum is simply dummy
                                                            text of the printing and typesetting industry. Lorem Ipsum
                                                            has been the industry's standard dummy text ever since the
                                                            1500s, when an unknown printer
                                                            took a
                                                            galley of type and scrambled it to make a type specimen
                                                            book.</p>
                                                        <a href="#!" class="m-r-30 text-muted"><i
                                                                    class="feather icon-thumbs-up m-r-15"></i>Helpful?</a>
                                                        <a href="#!"><i
                                                                    class="feather icon-heart-on text-c-red m-r-15"></i></a>
                                                        <a href="#!"><i class="feather icon-edit text-muted"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a class="carousel-control-prev carousel-control" href="#carouseltestimonials"
                                   role="button"
                                   data-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span><span
                                            class="sr-only">Previous</span></a>
                                <a class="carousel-control-next carousel-control" href="#carouseltestimonials"
                                   role="button"
                                   data-slide="next"><span class="carousel-control-next-icon" aria-hidden="true"></span><span
                                            class="sr-only">Next</span></a>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Participation Proof -->

                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Participation Proof</h5>
                        </div>
                        <div class="card-body">
                            <div id="carouselParticipation" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="carousel-item active">
                                        <div class="row">
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card user-card user-card-1 support-bar1">
                                                    <div class="card-header border-0 p-2 pb-0">
                                                        <div class="cover-img-block">
                                                            <img src="{{asset('panel/assets/images/widget/slider7.jpg')}}"
                                                                 alt=""
                                                                 class="img-fluid">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card user-card user-card-1 support-bar1">
                                                    <div class="card-header border-0 p-2 pb-0">
                                                        <div class="cover-img-block">
                                                            <img src="{{asset('panel/assets/images/widget/slider7.jpg')}}"
                                                                 alt=""
                                                                 class="img-fluid">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card user-card user-card-1 support-bar1">
                                                    <div class="card-header border-0 p-2 pb-0">
                                                        <div class="cover-img-block">
                                                            <img src="{{asset('panel/assets/images/widget/slider7.jpg')}}"
                                                                 alt=""
                                                                 class="img-fluid">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card user-card user-card-1 support-bar1">
                                                    <div class="card-header border-0 p-2 pb-0">
                                                        <div class="cover-img-block">
                                                            <img src="{{asset('panel/assets/images/widget/slider7.jpg')}}"
                                                                 alt=""
                                                                 class="img-fluid">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="row">
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card user-card user-card-1 support-bar1">
                                                    <div class="card-header border-0 p-2 pb-0">
                                                        <div class="cover-img-block">
                                                            <img src="{{asset('panel/assets/images/widget/slider7.jpg')}}"
                                                                 alt=""
                                                                 class="img-fluid">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card user-card user-card-1 support-bar1">
                                                    <div class="card-header border-0 p-2 pb-0">
                                                        <div class="cover-img-block">
                                                            <img src="{{asset('panel/assets/images/widget/slider7.jpg')}}"
                                                                 alt=""
                                                                 class="img-fluid">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card user-card user-card-1 support-bar1">
                                                    <div class="card-header border-0 p-2 pb-0">
                                                        <div class="cover-img-block">
                                                            <img src="{{asset('panel/assets/images/widget/slider7.jpg')}}"
                                                                 alt=""
                                                                 class="img-fluid">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6">
                                                <div class="card user-card user-card-1 support-bar1">
                                                    <div class="card-header border-0 p-2 pb-0">
                                                        <div class="cover-img-block">
                                                            <img src="{{asset('panel/assets/images/widget/slider7.jpg')}}"
                                                                 alt=""
                                                                 class="img-fluid">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselParticipation"
                                   role="button"
                                   data-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span><span
                                            class="sr-only">Previous</span></a>
                                <a class="carousel-control-next" href="#carouselParticipation"
                                   role="button"
                                   data-slide="next"><span class="carousel-control-next-icon" aria-hidden="true"></span><span
                                            class="sr-only">Next</span></a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Address & Subscribe -->

                <div class="col-xl-6">
                    <div class="card">
                        <div class="card-header">
                            <h5>Address</h5>
                        </div>
                        <div class="card-body">
                            <div class="col-md-6">
                                <div class="media">
                                    <i class="feather icon-map-pin mr-2 mt-1 f-18"></i>
                                    <div class="media-body">
                                        <p class="mb-0 text-muted">4289 Calvin Street</p>
                                        <p class="mb-0 text-muted">Baltimore, near MD Tower Maryland,</p>
                                        <p class="mb-0 text-muted">Maryland (21201)</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-6">
                    <div class="card">
                        <div class="card-header">
                            <h5>Subscribe</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-4">
                                    <img src="{{asset('panel/assets/images/pages/medal-gold.svg')}}" alt=""
                                         class="img-fluid w-50" style="max-height: 54px;">
                                </div>
                                <div class="col-xl-4 mt-2">
                                    <button class="btn btn-success btn-sm btn-round">Subscribe</button>
                                </div>
                                <div class="col-xl-4 mt-2">
                                    <button class="btn btn-primary btn-sm"><i class="fas fa-money-bill-alt"
                                                                              aria-hidden="true"></i></button>
                                    <button class="btn btn-primary btn-sm"><i class="fas fa-money-check"
                                                                              aria-hidden="true"></i></button>
                                    <button class="btn btn-primary btn-sm"><i class="fas fa-money-check-alt"
                                                                              aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </div>

    </div>
@endsection
@section('script')
    <style>
        a.carousel-control:hover {
            background: #bae08e;
            width: 5%;
        }
    </style>
@endsection