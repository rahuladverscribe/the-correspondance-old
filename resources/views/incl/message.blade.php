
@if(count($errors) > 0)
            @foreach($errors->all() as $error)
                <div class="alert alert-danger" role="alert">
                    <div class="alert-message">
                        <span>{{$error}} !</span>
                    </div>
                </div>
            @endforeach
@endif


@if(session('success'))

    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <div class="alert-message">
            <span><strong><i class="fa fa-check"></i> Success!</strong> {{session('success')}} !</span>
        </div>
    </div>

@endif


@if(session('error'))

    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <div class="alert-message">
            <span><strong><i class="fa fa-times"></i> Failed!</strong> {{session('error')}} !</span>
        </div>
    </div>

@endif