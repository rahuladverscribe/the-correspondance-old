@extends('layouts.auth')

@section('content')
    <div class="col-md-12">
        <div class="card-body">
            {{--<img src="{{asset('panel/assets/images/logo-dark.png')}}" alt="" class="img-fluid mb-4">--}}
            <h4 style="color: #4680ff;">The Correspondance</h4>
            <h4 class="mb-3 f-w-400">{{ __('Login') }}</h4>

            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group mb-3">
                    <label class="floating-label" for="Email">{{ __('E-Mail Address or Phone') }}</label>
                    <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email"
                           value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label class="floating-label" for="Password">{{ __('Password') }}</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"
                           required autocomplete="current-password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="custom-control custom-checkbox text-left mb-4 mt-2">
                    <input type="checkbox" name="remember" class="custom-control-input" id="customCheck1" {{ old('remember') ? 'checked' : '' }}>
                    <label class="custom-control-label" for="customCheck1">Save credentials.</label>
                </div>
                <button type="submit" class="btn btn-block btn-primary mb-4">{{ __('Login') }}</button>
            </form>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('panel/assets/js/plugins/jquery.validate.min.js')}}"></script>

    <script>
        $(document).ready(function() {
            $("#tcpd-form").validate({
                rules: {
                    name: {
                        required: true
                    },
                    description: {
                        required: true
                    }
                },errorPlacement: function errorPlacement(error, element) {
                    var $parent = $(element).parents('.form-group');

                    // Do not duplicate errors
                    if ($parent.find('.jquery-validation-error').length) {
                        return;
                    }

                    $parent.append(
                        error.addClass('jquery-validation-error small form-text invalid-feedback')
                    );
                },
                highlight: function(element) {
                    var $el = $(element);
                    var $parent = $el.parents('.form-group');

                    $el.addClass('is-invalid');

                    // Select2 and Tagsinput
                    if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
                        $el.parent().addClass('is-invalid');
                    }
                },
                unhighlight: function(element) {
                    $(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
                }
            });
        });
    </script>
    <style>
        .form-group .floating-label {
            top: -10px;
            font-size: 0.75rem;
            color: #4680ff;
        }
    </style>
@endsection
