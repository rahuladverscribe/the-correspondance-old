<ul class="nav pcoded-inner-navbar ">
    <li class="nav-item pcoded-menu-caption">
        <label></label>
    </li>
    <li class="nav-item">
        <a href="{{route('dashboard')}}" class="nav-link "><span class="pcoded-micon"><i
                        class="feather icon-home"></i></span><span class="pcoded-mtext">Home</span></a>
    </li>
    <li class="nav-item">
        <a href="#" class="nav-link ">
        <span class="pcoded-micon">
            <i class="feather icon-user"></i>
        </span>
            <span class="pcoded-mtext">Profile</span></a>
    </li>
    <li class="nav-item">
        <a href="#" class="nav-link ">
        <span class="pcoded-micon">
            <i class="feather icon-layers"></i>
        </span>
            <span class="pcoded-mtext">Levels</span></a>
    </li>
    <li class="nav-item pcoded-hasmenu">
        <a href="#!" class="nav-link ">
        <span class="pcoded-micon"><i class="feather icon-sliders"></i></span><span class="pcoded-mtext">Your Jobwork</span>
        </a>
        <ul class="pcoded-submenu">
            <li><a href="#">Scratch Card</a></li>
            <li><a href="#">Videos (Paid)</a></li>
            <li><a href="#">Bidding</a></li>
        </ul>
    </li>
    <li class="nav-item">
        <a href="#" class="nav-link ">
        <span class="pcoded-micon">
            <i class="feather icon-server"></i>
        </span>
            <span class="pcoded-mtext">Business Details</span></a>
    </li>
    <li class="nav-item pcoded-hasmenu">
        <a href="#!" class="nav-link ">
            <span class="pcoded-micon"><i class="feather icon-upload-cloud"></i></span><span class="pcoded-mtext">Upload</span>
        </a>
        <ul class="pcoded-submenu">
            <li><a href="#">Your Uploads</a></li>
            <li><a href="#">Your Article</a></li>
            {{--<li><a href="#">Your Video</a></li>--}}
            <li><a href="#">Testimonials</a></li>
        </ul>
    </li>
    <li class="nav-item pcoded-hasmenu">
        <a href="#!" class="nav-link ">
            <span class="pcoded-micon"><i class="feather icon-loader"></i></span><span class="pcoded-mtext">Advertise with Us</span>
        </a>
        <ul class="pcoded-submenu">
            <li><a href="#">Uploads Images</a></li>
            <li><a href="#">Uploads Videos</a></li>
        </ul>
    </li>
</ul>