<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'TheCorrespondence') }}</title>

    <link rel="icon" href="{{asset('panel/assets/images/favicon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('panel/assets/css/style.css')}}">

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
</head>
<body class="background-green">

<div class="loader-bg">
    <div class="loader-track">
        <div class="loader-fill"></div>
    </div>
</div>


<nav class="pcoded-navbar navbar-collapsed menu-light brand-blue">
    <div class="navbar-wrapper  ">
        <div class="navbar-content scroll-div ">
            @if(auth()->user())
                <div class="">
                    <div class="main-menu-header">
                        <img class="img-radius" src="{{asset('storage/'. auth()->user()->image)}}"
                             alt="User-Profile-Image">
                        <div class="user-details">
                            <div id="more-details">{{auth()->user()->name}} <i class="fa fa-caret-down"></i></div>
                        </div>
                    </div>
                    <div class="collapse" id="nav-user-link">
                        <ul class="list-inline">
                            <li class="list-inline-item"><a href="#" data-toggle="tooltip"
                                                            title="View Profile"><i class="feather icon-user"></i></a>
                            </li>
                            <li class="list-inline-item"><a href="{{route('logout')}}" data-toggle="tooltip"
                                                            title="Logout"
                                                            class="text-danger"><i class="feather icon-power"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            @else
                <div class="main-menu-header">
                    <img class="img-radius" src="{{asset('storage/user.png')}}" alt="User-Profile-Image">
                    <div class="user-details">
                        <div id="more-details"><i class="feather icon-log-in"></i> Login</div>
                    </div>
                </div>
            @endif
            @if(auth()->user())
                @switch(auth()->user()->role)
                    @case('1')
                    @include('layouts.navigation.superadmin')
                    @break
                    @case('2')
                    @include('layouts.navigation.admin')
                    @break
                    {{--@case('3')--}}
                    {{--@include('layouts.navigation.user')--}}
                    {{--@break--}}
                    @default
                    @include('layouts.navigation.user')
                @endswitch
            @else
                @include('layouts.navigation.guest')
            @endif

        </div>
    </div>
</nav>

<header class="navbar pcoded-header navbar-expand-lg navbar-light header-blue">
    <div class="m-header">
        <a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
        <a href="{{route('dashboard')}}" class="b-brand">
            <h5 style="margin-top: 10px; color: white;">The Correspondence</h5>
            {{--<img src="assets/images/logo.png" alt="" class="logo">--}}
            {{--<img src="assets/images/logo-icon.png" alt="" class="logo-thumb">--}}
        </a>
        <a href="#!" class="mob-toggler">
            <i class="feather icon-more-vertical"></i>
        </a>
    </div>
    <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto">
            <li>
                <div class="dropdown drp-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="feather icon-user"></i>
                    </a>
                    @if(auth()->user())
                        <div class="dropdown-menu dropdown-menu-right profile-notification">
                            <div class="pro-head">
                                <img src="{{asset('storage/'. auth()->user()->image)}}" class="img-radius"
                                     alt="User-Profile-Image">
                                <span>{{auth()->user()->name}}</span>
                                <a href="{{route('logout')}}" class="dud-logout" title="Logout">
                                    <i class="feather icon-log-out"></i>
                                </a>
                            </div>
                            <ul class="pro-body">
                                <li><a href="#" class="dropdown-item"><i class="feather icon-user"></i>
                                        Profile</a></li>
                                <li><a href="#" class="dropdown-item"><i class="feather icon-mail"></i>
                                        My Messages</a></li>
                                <li><a href="{{route('logout')}}" class="dropdown-item"><i
                                                class="feather icon-lock"></i>
                                        Lock Screen</a></li>
                            </ul>
                        </div>
                    @endif
                </div>
            </li>
        </ul>
    </div>


</header>

@yield('content')
@if(!auth()->check())
    <div class="modal fade" id="loginForm" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Login with Us : </h5>
                </div>
                <form method="POST" id="tcpd-form" action="{{ route('login') }}">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Email or Phone :</label>
                            <input type="text" class="form-control @error('email') is-invalid @enderror" name="email"
                                   placeholder="Email or Phone" value="{{ old('email') }}" autocomplete="email"
                                   required>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Password :</label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror"
                                   name="password" placeholder="Password" required autocomplete="current-password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="checkbox" name="remember" class="cform-control"
                                   id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label for="customCheck1">Save credentials.</label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Login Now</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endif

<script src="{{asset('panel/assets/js/vendor-all.min.js')}}"></script>
<script src="{{asset('panel/assets/js/plugins/bootstrap.min.js')}}"></script>
<script src="{{asset('panel/assets/js/ripple.js')}}"></script>
<script src="{{asset('panel/assets/js/pcoded.min.js')}}"></script>
{{--<script src="{{asset('panel/assets/js/menu-setting.min.js')}}"></script>--}}
<script src="{{asset('panel/assets/js/plugins/apexcharts.min.js')}}"></script>
<script src="{{asset('panel/assets/js/pages/dashboard-project.js')}}"></script>
<script src="{{asset('panel/assets/js/plugins/jquery.validate.min.js')}}"></script>
<script>
    var userCheck = {{ auth()->check() ? 'true' : 'false' }}
    $(document).ready(function () {
        if (!userCheck) {
            $("#loginForm").modal({backdrop: 'static', keyboard: false});
        }
    });
</script>
<script>
    $(document).ready(function () {
        $("#tcpd-form").validate({
            rules: {
                name: {
                    required: true
                },
                description: {
                    required: true
                }
            }, errorPlacement: function errorPlacement(error, element) {
                var $parent = $(element).parents('.form-group');

                // Do not duplicate errors
                if ($parent.find('.jquery-validation-error').length) {
                    return;
                }

                $parent.append(
                    error.addClass('jquery-validation-error small form-text invalid-feedback')
                );
            },
            highlight: function (element) {
                var $el = $(element);
                var $parent = $el.parents('.form-group');

                $el.addClass('is-invalid');

                // Select2 and Tagsinput
                if ($el.hasClass('select2-hidden-accessible') || $el.attr('data-role') === 'tagsinput') {
                    $el.parent().addClass('is-invalid');
                }
            },
            unhighlight: function (element) {
                $(element).parents('.form-group').find('.is-invalid').removeClass('is-invalid');
            }
        });
    });
</script>
@yield('script')

</body>
</html>
