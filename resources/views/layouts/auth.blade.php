<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'HeatBird') }}</title>

    <link rel="icon" href="{{asset('panel/assets/images/favicon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('panel/assets/css/style.css')}}">
</head>
<body>

<div class="auth-wrapper">
    <div class="auth-content">
        <div class="card">
            <div class="row align-items-center text-center">
                @yield('content')
            </div>
        </div>
    </div>
</div>

<script src="{{asset('panel/assets/js/vendor-all.min.js')}}"></script>
<script src="{{asset('panel/assets/js/plugins/bootstrap.min.js')}}"></script>
<script src="{{asset('panel/assets/js/ripple.js')}}"></script>
<script src="{{asset('panel/assets/js/pcoded.min.js')}}"></script>
@yield('script')
</body>
</html>
