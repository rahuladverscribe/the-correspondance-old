<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function(){
});

Auth::routes(['register' => false, 'reset' => false, 'verify' => false,]);

Route::get('register', function (){
    return redirect(route('login'));
});

Route::get('/', 'panel\DashboardController@index')->name('dashboard');

Route::get('home', function (){
    return redirect(route('dashboard'));
});

Route::get('logout', 'Auth\LoginController@logout')->name('logout');
