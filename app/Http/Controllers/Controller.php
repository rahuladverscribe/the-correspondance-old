<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function generateSlug($title, $table) {
        $slug = Str::slug($title);
        $slugList = DB::table($table)
            ->where('slug', 'like', $slug.'%')
            ->get();
        $slugCount = $slugList->count();
        if($slugCount) {
            $slugCount = $slugCount;
            return $slug . '-' .$slugCount;
        } else {
            return $slug;
        }
    }
}
